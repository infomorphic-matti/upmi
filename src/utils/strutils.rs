//! String utility functions and extension traits.

/// [`str`] extension trait. Only implement on [`str`]
pub(crate) trait StrExt {
    /// Find the first character matching the given criteria. If found, then return the character
    /// that matched. If not found, return [`None`]
    fn find_first_char_matching(&self, criteria: impl FnMut(char) -> bool) -> Option<char>;
}

impl StrExt for str {
    #[inline]
    fn find_first_char_matching(&self, criteria: impl FnMut(char) -> bool) -> Option<char> {
        // Locate boundary byte index
        let first_matching_start = self.find(criteria)?;
        // We can't grab characters directly - however, we've found the starting byte index of the
        // character and can cut the string so the first character in it is the one we're looking
        // for.
        let self_with_character_at_start = &self[first_matching_start..];
        self_with_character_at_start.chars().next()
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn first_chars() {
        assert_eq!(
            "leonard davinci".find_first_char_matching(|c| matches!(c, 'a' | 'b')),
            Some('a')
        );

        assert_eq!("WHEEE".find_first_char_matching(char::is_whitespace), None);
        assert_eq!(
            "it is time. to RULE THE WORLD!".find_first_char_matching(char::is_uppercase),
            Some('R')
        );
    }
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
