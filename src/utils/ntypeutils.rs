//! Contains traits to ease implementing small newtypes with optional user validation and custom
//! errors (mainly [`crate::packages::PackageName`] and [`crate::packages::PackageVersion`] at the
//! minute)

pub(crate) mod cratesealed {
    use crate::commontypes::SString;
    use core::borrow::Borrow;

    /// Trait upon which other extension traits can be applied later automatically.
    pub trait SStringNewtype: Borrow<str> {
        /// Extract the inner data
        fn into_inner_sstring(self) -> SString;

        /// Construct a new value from the smallstr
        fn from_sstring(value: SString) -> Self;

        /// Get a string reference
        #[inline]
        fn as_str(&self) -> &str {
            self.borrow()
        }

        /// Create this directly from an &str
        #[inline]
        fn from_str_ref(s: &str) -> Self
        where
            Self: Sized,
        {
            Self::from_sstring(SString::from_str(s))
        }
    }
}

pub(crate) use cratesealed::SStringNewtype;

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
