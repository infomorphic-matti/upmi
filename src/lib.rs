//! `upmi` - Universal Package Manager Interface/Invoker
//!
//! This library is designed to enable uniform, programmatic access to system package managers
//! (such as `pacman`, `apt`, `deb`, `winget`, etc.). It is currently designed to use their CLI,
//! but should be extensible enough to use ones that provide system libraries (such as `alpm`) in
//! future.
//!
//! Much information on the actual invokation syntax has been done by the [`upt`][upt] crate, for
//! which I'm grateful.
//!
//!
//! [upt]: https://github.com/sigoden/upt/tree/main
pub mod commontypes;
pub mod managers;
pub mod packages;
pub(crate) mod utils;
pub mod vendor;

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
