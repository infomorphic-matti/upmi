//! Tools to actually invoke package managers.
//!
//! Actually invoking package managers is complex. In particular, managing the input and output is
//! difficult, because users may want to do different things - such as simply inheriting the main
//! program's output, or capturing and analyzing it. In future, we might even want to be able to
//! use package manager shared libs instead of calling out to CLI.
//!
//! To handle this, we create a notion of [`InvokationMode`][im] and [`InvokeWith`][iw]. In combination,
//! these allow:
//! * A generic configuration function with uniform "wrapped" conceptual output type that may or
//!   may not actually reify into an output type.
//! * The ability to apply transforms (maps) over the wrapped version of that type
//!
//! Traits for the various actions that can be made possible for a package manager can be found
//! inside [`actions`]. The common structure is generally as follows:
//! * An error type, which is parameterised on InvokationError, some custom fallback error, and the
//!     package version descriptor type. All but the first have sensible defaults. This error
//!     should contain relevant sub-errors e.g. if there is potential validation (either of user
//!     input or package manager output), then one of the [`ValidationError`][ve],
//!     [`PackageNameError`][pnpe], or [`PackageVersionParseError`][pvpe] should be used,
//!     preferring the first as it gives more flexibility.
//! * An output type for the case where you intend to analyse the output rather than forwarding
//!     stdio or blindly passing on the output string.
//! * A type containing various options for the operation. When implementing, you'll want to make
//!     sure of the following:
//!     * `#[derive(serde::Serialize, serde::Deserialize)]` and `#[serde(rename_all = "kebab-case")]`
//!         are present on the type, conditional on `feature = serde`. So, the following two
//!         attributes should be present:
//!             * `#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]`
//!             * `#[cfg_attr(feature = "serde", serde(rename_all = "kebab-case"))]`
//!     * `#[derive(Debug, Clone)]` should be present (and if it makes sense,
//!         `#[derive(Debug, Clone, Copy)]`
//!     * If the options parameter contains versions, then it should be parameterised on the
//!         version type.
//! * An error alias to the error type described earlier, that makes it easy to refer to the errors
//!   produced by invoking the action with a given invokable + mode, custom error, and version.
//! * A trait for the actual operation. This is typically a trait with a couple of associated types
//!   and one or more functions:
//!   * An `InvokerConfig` Generic Associated Type - this is the type which should implement
//!     [`InvokeWith`][iw] for various [`InvokationMode`][im]s, and it's how you take the
//!     parameters for an operation and convert that into something which can produce a (wrapped
//!     and maybe phantasmal) output.
//!   * An `InvokerConfigConstructionData` Generic Associated Type, that only needs to be valid
//!     when the `InvokerConfig` implements [`InvokeWith`][iw] for it's [`InvokationMode`][im]
//!     parameter. This, along with the relevant operation parameters and any internal package
//!     manager information can be used to create the [`InvokeWith::UserProvidedInfo`][iw_upi]
//!     needed to actually invoke the invokable configuration.
//!   * One or more methods for the actual operation. Typically, the outputs of this are wrapped in
//!     the invokation mode's type wrapper, which means that the internal implementation can do
//!     uniform analysis processing via map, even for cases where there's no "real" output.
//!
//! [im]: `invokation_mode::InvokationMode`
//! [iw]: `invoker_config::InvokeWith`
//! [iw_upi]: `invoker_config::InvokeWith::UserProvidedInfo`
//!
//! [pnpe]: `crate::packages::name::PackageNameError`
//! [pvpe]: `crate::packages::version::PackageVersionError`
//! [ve]: `super::validating::ValidationError`

pub mod actions;
pub mod commonflags;
pub mod invokation_mode;
pub mod invoker_config;

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
