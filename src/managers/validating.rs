//! Module allowing package managers to define how they validate package names and versions.
//!
//! This provides a trait for package managers to implement that lets them provide validation of
//! package names and versions. For more information, see [`crate::packages`].

use crate::packages::{
    name::PackageNameError,
    version::PackageVersionError,
    with_packages::{self, UnvalidatedPackageInfo, Validated},
    PackageVersion,
};

/// Error when attempting to validate a package name or version, with custom backing error.
///
/// This error type lets you add extra information in an error message. It provides convenience
/// methods to let you construct its variants with and without extra info, as well as [`From`]
/// impls that provide an empty message by default to ease the use of `?`.
#[derive(Debug, thiserror::Error)]
pub enum ValidationError<Custom = anyhow::Error, Version = PackageVersion> {
    /// Indicate an error when parsing package names. Optionally, also lets you add extra info.
    ///
    /// The message form is `invalid package name:{extra info}:{source error msg}`, so you can, for
    /// instance, indicate that the error was while parsing from package manager output, or from
    /// user input, or other such things.
    #[error("invalid package name:{extra_info}:{source}")]
    PackageNameParse {
        #[source]
        source: PackageNameError<Custom>,
        extra_info: &'static str,
    },
    /// Indicate an error when parsing package versions. Optionally, also lets you add extra info.
    ///
    /// The message form is `invalid package version:{extra info}:{source error msg}`, so you can, for
    /// instance, indicate that the error was while parsing from package manager output, or from
    /// user input, or other such things.
    #[error("invalid package version:{extra_info}:{source}")]
    PackageVersionParse {
        #[source]
        source: PackageVersionError<Custom, Version>,
        extra_info: &'static str,
    },
    /// Other custom error.
    #[error(transparent)]
    Other(Custom),
}

impl<Custom, Version> ValidationError<Custom, Version> {
    /// Create a validation error from the name parsing error, with no special extra info
    #[inline]
    pub const fn name_parse_err(source: PackageNameError<Custom>) -> Self {
        Self::name_parse_err_with_info(source, "")
    }

    /// Create a validation error from the name parsing error, with an extra info message. See
    /// [`ValidationError::PackageNameParse`] for more information.
    #[inline]
    pub const fn name_parse_err_with_info(
        source: PackageNameError<Custom>,
        extra_info: &'static str,
    ) -> Self {
        Self::PackageNameParse { source, extra_info }
    }

    /// Create a validation error from the version parsing error, with no special extra info
    #[inline]
    pub const fn version_parse_err(source: PackageVersionError<Custom, Version>) -> Self {
        Self::version_parse_err_with_info(source, "")
    }

    /// Create a validation error from the version parsing error, with an extra info message. See
    /// [`ValidationError::PackageVersionParse`] for more information.
    #[inline]
    pub const fn version_parse_err_with_info(
        source: PackageVersionError<Custom, Version>,
        extra_info: &'static str,
    ) -> Self {
        Self::PackageVersionParse { source, extra_info }
    }

    #[inline]
    pub const fn custom_err(c: Custom) -> Self {
        Self::Other(c)
    }
}

impl<Custom, Version> From<PackageNameError<Custom>> for ValidationError<Custom, Version> {
    #[inline]
    fn from(value: PackageNameError<Custom>) -> Self {
        Self::name_parse_err(value)
    }
}

// Implement custom methods on PackageNameError
impl<Custom> PackageNameError<Custom> {
    /// Turn this [`PackageNameError`] into a [`ValidationError`], with no extra info message.
    #[inline]
    pub const fn into_validation_err<V>(self) -> ValidationError<Custom, V> {
        ValidationError::name_parse_err(self)
    }

    /// Turn this [`PackageNameError`] into a [`ValidationError`] with an `extra_info` message
    /// (e.g. to indicate where the invalid package name came from).
    #[inline]
    pub const fn into_validation_err_with_message<V>(
        self,
        extra_info: &'static str,
    ) -> ValidationError<Custom, V> {
        ValidationError::name_parse_err_with_info(self, extra_info)
    }
}

impl<Custom, Version> From<PackageVersionError<Custom, Version>>
    for ValidationError<Custom, Version>
{
    #[inline]
    fn from(value: PackageVersionError<Custom, Version>) -> Self {
        Self::version_parse_err(value)
    }
}

// Implement custom methods on PackageVersionError
impl<Custom, V> PackageVersionError<Custom, V> {
    /// Turn this [`PackageVersionError`] into a [`ValidationError`], with no extra info message.
    #[inline]
    pub const fn into_validation_err(self) -> ValidationError<Custom, V> {
        ValidationError::version_parse_err(self)
    }

    /// Turn this [`PackageVersionError`] into a [`ValidationError`] with an `extra_info` message
    /// (e.g. to indicate where the invalid package version came from).
    #[inline]
    pub const fn into_validation_err_with_message(
        self,
        extra_info: &'static str,
    ) -> ValidationError<Custom, V> {
        ValidationError::version_parse_err_with_info(self, extra_info)
    }
}

/// Package manager trait that lets a package manager validate package names and versions.
pub trait Validating<CustomErr = anyhow::Error, Version = PackageVersion> {
    /// Validate a collection of unvalidated package names or versions or both. If they're all OK,
    /// then return [`Ok`], but if not, then emit a [`ValidationError`].
    fn validate<'a>(
        &self,
        v: impl IntoIterator<Item = with_packages::Unvalidated<'a, Version>>,
    ) -> Result<(), ValidationError<CustomErr, Version>>
    where
        Version: 'a;
}

/// Extension trait to [`Validating`] that provides utility methods (on your own package manager)
/// for handling validation.
pub trait ValidatingExt<Custom = anyhow::Error, Version = PackageVersion>:
    Validating<Custom, Version>
{
    /// Validate the given data.
    ///
    /// Note that, while this takes the data by-value, you should be able to pass in a reference to
    /// the data just fine, as [`UnvalidatedPackageInfo`] has blanket impls for references.
    fn validate_data<WithUnvalidatedPackageInfo: UnvalidatedPackageInfo<Version>>(
        &self,
        data: WithUnvalidatedPackageInfo,
    ) -> Result<Validated<WithUnvalidatedPackageInfo>, ValidationError<Custom, Version>> {
        self.validate(data.unvalidated_package_names_and_versions())?;
        // # Safety: just validated
        Ok(unsafe { Validated::new(data) })
    }
}

impl<Custom, Version, T: Validating<Custom, Version>> ValidatingExt<Custom, Version> for T {}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
