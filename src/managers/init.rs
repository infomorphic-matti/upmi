//! Traits and infrastructure for package managers to locate themselves and initialize internal
//! access state.

use crate::commontypes::SString;

/// Error when constructing a package manager from system information.
///
/// Several in-built options are provided, as well as a fallback to (by default)
/// [`anyhow::Error`]
#[derive(Debug, thiserror::Error)]
pub enum PackageManagerCreationError<Fallback = anyhow::Error> {
    #[error("could not locate binary: '{0}'")]
    CouldNotLocateBinary(SString),
    #[error("io error: {0}")]
    IoError(#[source] std::io::Error),
    #[error(transparent)]
    Other(#[from] Fallback),
}

/// Package manager handle that can be created from a reference to some system instance information
///
/// System instance information structures can be found in [`crate::vendor::instance`]
pub trait PackageManager<
    'sys,
    SystemInstanceInformation: ?Sized + 'sys,
    ErrFallback = anyhow::Error,
>
{
    /// Information needed to locate and initialize the package manager.
    ///
    /// This is where you'd put things like library configuration. If this implements
    /// [`Default::default`], then you can use the [`PackageManagerExt`] extension trait for a
    /// simpler and more uniform initialisation interface.
    type InitConfiguration: Sized;

    /// Create all the core data required to work with the package manager, from the provided
    /// system information and extra init config.
    fn from_system_information_and_config(
        system_information: &'sys SystemInstanceInformation,
        initialization_config: Self::InitConfiguration,
    ) -> Result<Self, PackageManagerCreationError<ErrFallback>>
    where
        Self: Sized;
}

/// Extension trait for [`PackageManager`], providing a more uniform interface.
pub trait PackageManagerExt<
    'sys,
    SystemInstanceInformation: ?Sized + 'sys,
    ErrFallback = anyhow::Error,
>: PackageManager<'sys, SystemInstanceInformation, ErrFallback>
{
    /// Initialize the package manager from the system information, using the default
    /// configuration.
    #[inline]
    fn from_system_information_and_default(
        system_information: &'sys SystemInstanceInformation,
    ) -> Result<Self, PackageManagerCreationError<ErrFallback>>
    where
        Self: Sized,
        Self::InitConfiguration: Default,
    {
        Self::from_system_information_and_config(system_information, Default::default())
    }
}

impl<
        'sys,
        SystemInstanceInformation: ?Sized + 'sys,
        ErrFallback,
        T: PackageManager<'sys, SystemInstanceInformation, ErrFallback> + ?Sized,
    > PackageManagerExt<'sys, SystemInstanceInformation, ErrFallback> for T
{
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
