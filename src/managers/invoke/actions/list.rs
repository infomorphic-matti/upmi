//! Module for listing installed packages in a package manager.
use std::collections::HashMap;

use crate::{
    managers::{invoke::commonflags::ConfirmFlag, validating::ValidationError},
    packages::{PackageName, PackageVersion},
};

use super::super::{
    invokation_mode::InvokationMode,
    invoker_config::{InvokeWith, InvokeWithFull},
};

/// Configuration parameters for [`Listing`] commands.
#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(rename_all = "kebab-case"))]
pub struct ListingConfig {
    /// Whether or not to autoconfirm
    pub confirm: ConfirmFlag,
}

/// Output of listings when parsed by the implementation.
///
/// Map of installed package names to their versions.
pub type ListingOutput<Version = PackageVersion> = HashMap<PackageName, Version>;

/// Error when attempting to perform a [`Listing`]
#[derive(Debug, thiserror::Error)]
pub enum ListingError<InvokationError, Custom = anyhow::Error, Version = PackageVersion> {
    #[error("validation error during listing: {0}")]
    Validation(
        #[from]
        #[source]
        ValidationError<Custom, Version>,
    ),
    #[error("failed to perform package manager invokation: {0}")]
    Invokation(InvokationError),
    #[error(transparent)]
    Other(Custom),
}

/// Shorthand for the error type produced when invoking the action.
pub type ListingErrorInvoked<InvokedWithTy, Mode, CustomErr, Ver> =
    ListingError<<InvokedWithTy as InvokeWith<Mode>>::Error, CustomErr, Ver>;

pub type ListingResult<InvokedWithTy, Mode, CustomErr, Ver> = Result<
    InvokeWithFull<Mode, ListingOutput<Ver>>,
    ListingErrorInvoked<InvokedWithTy, Mode, CustomErr, Ver>,
>;

/// List all the packages (either installed or any).
pub trait Listing<Err = anyhow::Error, Version = PackageVersion> {
    /// [`InvokeWith`] implementor, for whichever modes you want to support.
    type InvokerConfig<'s>
    where
        Self: 's;

    /// Arbitrary Construction data that can be used in combination with [`ListingConfig`] to
    /// eventually build an InvokerConfig.
    ///
    /// This is what users see when calling [`Listing::list_packages`]. Internally, you can use
    /// this along with [`ListingConfig`] to invoke your [`Listing::InvokerConfig`] with a combined
    /// [`InvokeWith::UserProvidedInfo`].
    type InvokerConfigConstructionData<'s, Mode: InvokationMode>
    where
        Self: 's,
        Self::InvokerConfig<'s>: InvokeWith<Mode>;

    /// List the installed packages - along with their version information - for this package manager.
    fn list_installed_packages<'s, Mode: InvokationMode>(
        &'s mut self,
        invoker_config_ctor: Self::InvokerConfigConstructionData<'s, Mode>,
        listing_parameters: ListingConfig,
    ) -> ListingResult<Self::InvokerConfig<'s>, Mode, Err, Version>
    where
        Self::InvokerConfig<'s>: InvokeWith<Mode>;
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
