//! Traits and structure provided by package managers to things wanting to configure how to invoke
//! them.

use super::invokation_mode::InvokationMode;

/// Utility type containing the "final" main mapped output for something implementing
/// [`InvokeWith`]. Assumes no mapping/transform.
pub type InvokeWithUnmappedFinal<Mode, Invoke> =
    InvokeWithFinal<Mode, <Invoke as InvokeWith<Mode>>::InvokationOutput>;

/// Utility type containing the "final" main unmapped output for something implementing
/// [`InvokeWith`] as well as the extra invokation data to allow further user configuration.
///
/// Assumes no applied transforms to the output.
pub type InvokeWithUnmappedFull<Mode, Invoke> =
    InvokeWithFull<Mode, <Invoke as InvokeWith<Mode>>::InvokationOutput>;

/// Utility type containing a "final" output for something implementing [`InvokationMode`], that has
/// had it's output altered.
pub type InvokeWithFinal<Mode, T> = <Mode as InvokationMode>::FinalOutput<T>;

/// Utility type containing a "final" output for something implementing [`InvokationMode`], that
/// has had it's output altered.
pub type InvokeWithFull<Mode, T> = (
    InvokeWithFinal<Mode, T>,
    <Mode as InvokationMode>::ExtraInvokationData,
);

/// Trait to enable invokation with a given mode. Is essentially something "monad-ish"
pub trait InvokeWith<Mode: InvokationMode> {
    /// Configuration for this mode - in particular, any options you might want to provide the user
    /// for controlling invokation (such as, for instance, grabbing the output stream and
    /// processing it as-wanted) should go here.
    type UserProvidedInfo;

    /// Output of a hypothetical invokation. Note that this is wrapped in `Self::FinalOutput` - as
    /// such, this is more a conceptual thing. Whether it is actually present at any point is
    /// defined via [`InvokationMode::FinalOutput`], it is mostly an abstraction to allow uniform
    /// application of analysis via [`InvokationMode::map`].
    type InvokationOutput;

    /// Error when constructing the invokation.
    type Error;

    /// Construct an invokation using the configuration, providing the wrapped invokation as well
    /// as extra data that can only be made in the construction of the invokation to allow users to
    /// handle it's output.
    fn invoke(
        self,
        info: Self::UserProvidedInfo,
    ) -> Result<InvokeWithUnmappedFull<Mode, Self>, Self::Error>;
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
