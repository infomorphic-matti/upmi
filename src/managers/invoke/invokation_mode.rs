//! Provides traits on how to actually invoke things.

/// Define a mode by which invokation can occur. Generally, when implementing a package manager,
/// you'll want to implement [`super::invoker_config::InvokeWith`] for various [`InvokationMode`]s
/// available. In practise, you're likely to reuse something existing within this crate that alread
/// implements things.
///
/// The structures implementing this typically also have inherent impl functions to help with
/// managing the relevant [`InvokationMode::FinalOutput`] types.
///
/// This essentially forms a sort of monad-ish thing.
pub trait InvokationMode {
    /// Wrapped type, representing the "final output" of an invokation. This may or may not
    /// actually include the respective parameter type, but the respective parameter type is the
    /// "abstract" output of some invokation.
    type FinalOutput<AnalysisOutput>;

    /// Type returned when constructing a (maybe-uninvoked) [`InvokationMode::FinalOutput`] to
    /// allow configuring output processing. For example, this might be a [`std::process::Stdio`]
    /// for modes that entail providing an output stream to hook into other commands.
    ///
    /// For modes that are fully invoked with no extra config data beyond that demanded by the
    /// config, this is probably `()`.
    type ExtraInvokationData;

    /// Apply some transformation to a [`InvokationMode::FinalOutput`]
    fn map<A, B>(v: Self::FinalOutput<A>, map: impl FnOnce(A) -> B) -> Self::FinalOutput<B>;
}

/// Mode that inherits the stdio entirely from the parent process - no way to process any kind of
/// output or anything like that. It spits out a child process handle, so you need to explicitly
/// wait for it. If you want waiting to be handled by the invokation, see
/// [`InheritStdioImmediate`]
///
/// This implements [`InvokationMode`]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
pub struct InheritStdio;

impl InheritStdio {
    /// Configure a command to properly inherit stdin/err/out of the parent process before
    /// execution.
    pub fn configure_stdio_inherit(cmd: &mut std::process::Command) {
        cmd.stdin(std::process::Stdio::inherit())
            .stdout(std::process::Stdio::inherit())
            .stderr(std::process::Stdio::inherit());
    }
}

impl InvokationMode for InheritStdio {
    type FinalOutput<AnalysisOutput> = std::process::Child;

    type ExtraInvokationData = ();

    #[inline]
    fn map<A, B>(v: Self::FinalOutput<A>, _map: impl FnOnce(A) -> B) -> Self::FinalOutput<B> {
        v
    }
}

/// Inherit the stdio of the current process into some child process, and immediately run the
/// child process and wait for it's completion.
///
/// The final output is then an exit code. For this, you probably want
/// [`std::process::Command::spawn`]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
pub struct InheritStdioImmediate;

impl InheritStdioImmediate {
    /// Configure a command to properly inherit stdin/err/out of the parent process before
    /// execution.
    #[inline]
    pub fn configure_stdio_inherit(cmd: &mut std::process::Command) {
        InheritStdio::configure_stdio_inherit(cmd)
    }
}

impl InvokationMode for InheritStdioImmediate {
    type FinalOutput<AnalysisOutput> = std::process::ExitStatus;

    type ExtraInvokationData = ();

    #[inline]
    fn map<A, B>(v: Self::FinalOutput<A>, _map: impl FnOnce(A) -> B) -> Self::FinalOutput<B> {
        v
    }
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
