//! Contains package managers.
//!
//! This library provides an [enum][PackageManagers] to describe a package manager as a whole, as
//! well as having specific, instantiatable types for a given package manager, which are provided
//! with information on the vendor (and can implement traits to be locatable on various different
//! [crate::vendor::instance]s).
//!
//! Functionality for each package manager is then provided via trait implementations.
//!
//! The traits in the submodules of this module are intended to be friendly towards being wrapped
//! in a giant enum - so they may prefer more uniform types where typically you'd have slightly
//! more custom types.

pub mod init;
pub mod invoke;
pub mod tools;
pub mod validating;

/// Package manager names - see [upt][upt-package-manager] for where I got this
///
/// [upt-package-managers]: https://github.com/sigoden/upt/blob/main/src/vendor.rs
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(feature = "serde", serde(rename_all = "kebab-case"))]
pub enum PackageManagerName {
    Apk,
    Apt,
    Brew,
    Cards,
    Choco,
    Dnf,
    Emerge,
    Eopkg,
    Flatpak,
    Guix,
    NixEnv,
    Opkg,
    Pacman,
    Pkg,
    PkgTermux,
    Pkgman,
    Port,
    PrtGet,
    Scoop,
    SlackPkg,
    Snap,
    Urpm,
    Winget,
    Xbps,
    Yum,
    Zypper,
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
