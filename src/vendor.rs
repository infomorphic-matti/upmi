//! Submodule for vendor information (and detection, if the `identify-vendor` feature is enabled.
//!
//! Vendors are OSes like `arch`, `windows`, `mac`, `ubuntu`, etc. They are then categorised with
//! precise information (such as the root filesystem). As such, a [vendor *id*][id::VendorID]
//! refers to a type of system, and a [vendor *instance*][instance::InstanceInformation] refers to
//! a specific system (for instance, the current one, or an install inside a folder). 

#[cfg(feature = "identify-vendor")]
pub mod detect;
pub mod id;
pub mod instance;
pub mod known_ids;

pub use id::VendorID;

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
