//! [VendorID] type and implementation, as well as the [VendorIDError] type.

use crate::{commontypes::SString, utils::strutils::StrExt};
use core::{borrow::Borrow, fmt::Display, str::FromStr};

#[derive(Debug, Clone, thiserror::Error)]
pub enum VendorIDError {
    #[error("vendor id contained invalid character '{0}' - expected only 0-9, a-z, ., -, or _")]
    DisallowedCharacter(char),
    #[error("vendor id was empty")]
    EmptyVendorId,
}

impl VendorIDError {
    /// Validate that the given string is ok as a vendor id or provide the reason it isn't
    pub fn validate_string<T: AsRef<str> + ?Sized>(data: &T) -> Result<(), VendorIDError> {
        let string = data.as_ref();
        if string.is_empty() {
            return Err(VendorIDError::EmptyVendorId);
        };
        match string.find_first_char_matching(|c: char| {
            !(c.is_ascii_lowercase() || c.is_ascii_digit() || c == '.' || c == '-' || c == '_')
        }) {
            Some(invalid_char) => Err(Self::DisallowedCharacter(invalid_char)),
            None => Ok(()),
        }
    }
}

/// ID of a vendor. Must be non-empty, and more generally, follow the requirements of the `ID`
/// variable in the `os-release` format commonly used on linux systems.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(
    feature = "serde",
    derive(serde_with::SerializeDisplay, serde_with::DeserializeFromStr)
)]
pub struct VendorID(SString);

impl VendorID {
    #[inline]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl Display for VendorID {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.0.as_str())
    }
}

impl TryFrom<SString> for VendorID {
    type Error = VendorIDError;

    #[inline]
    fn try_from(value: SString) -> Result<Self, Self::Error> {
        VendorIDError::validate_string(&value)?;
        Ok(VendorID(value))
    }
}

impl TryFrom<String> for VendorID {
    type Error = VendorIDError;

    #[inline]
    fn try_from(value: String) -> Result<Self, Self::Error> {
        VendorIDError::validate_string(&value)?;
        Ok(VendorID(value.into()))
    }
}

impl TryFrom<&str> for VendorID {
    type Error = VendorIDError;

    #[inline]
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        VendorIDError::validate_string(value)?;
        Ok(VendorID(value.into()))
    }
}

impl FromStr for VendorID {
    type Err = VendorIDError;

    #[inline]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        VendorID::try_from(s)
    }
}

impl AsRef<str> for VendorID {
    #[inline]
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl Borrow<str> for VendorID {
    #[inline]
    fn borrow(&self) -> &str {
        self.as_ref()
    }
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
