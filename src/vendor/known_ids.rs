//! Known vendor IDs for various vendor categories.
//! Used to help classify things.
//!
//! Much of the information here is taken directly from [upt]
//!
//! [upt]: https://github.com/sigoden/upt/blob/main/src/vendor.rs

use super::{id::VendorIDError, VendorID};
use crate::{commontypes::SString, managers::PackageManagerName as PM};
use core::{fmt::Display, ops::ControlFlow, str::FromStr};

/// Define known vendors and likely package managers.
macro_rules! vendor_package_managers {
    (
        $(#[$main_meta:meta])*
        $vis:vis enum $enum_name:ident {
            // Create a collection of vendors and map them to some fixed, ordered collection of
            // package managers
            $({
                $($(#[$variant_docs:meta])* $variant_name:ident = $vendor_name:literal),*
            } => [$($pkg_manager:path),*]),*
        }
    ) => {
        $(#[$main_meta])*
        /// This enum does not implement [`core::borrow::Borrow`], because it does not have the
        /// same [`Hash`] as it's string representation.
        #[cfg_attr(feature = "serde", derive(
                serde_with::SerializeDisplay,
                serde_with::DeserializeFromStr
            )
        )]
        $vis enum $enum_name {
            $($(
                $(#[$variant_docs])*
                $variant_name,
            )*)*
            /// Vendor was not one of the known values.
            Unknown(VendorID)
        }

        // Implement detection based on the string.
        impl $enum_name {
            // Test function to ensure that all the specified vendor names are valid vendor ids
            #[cfg(test)]
            pub fn ensure_vendor_names_are_ok() {
                $($($vendor_name.parse::<VendorID>().expect("Invalid known vendor name");)*)*
            }


            /// Attempt to match the given raw string to a known value for this vendor enum.
            ///
            /// If it is not a known vendor, then this returns [`None`], even if it's a valid
            /// vendor ID this could hold.
            ///
            /// This is at least partially an internal function - you probably want something more
            /// like [`Self::try_from_vendor_str`] or similar. This function is useful to avoid
            /// allocations, however.
            #[inline]
            pub fn maybe_from_known_vendor_str(v: &str) -> Option<Self> {
                match v {
                    $($($vendor_name => Some(Self::$variant_name),)*)*
                    _ => None
                }
            }

            /// Attempt to get a known specific vendor in the relevant vendor category list
            /// directly from a [`str`]. If the vendor is not valid, this will emit a
            /// [`VendorIDError`]. This will also create a [`Self::Unknown`] for an unknown vendor.
            #[inline]
            pub fn try_from_vendor_str(v: &str) -> Result<Self, VendorIDError> {
                match Self::maybe_from_known_vendor_str(v) {
                    Some(s) => Ok(s),
                    None => VendorID::try_from(v).map(Self::Unknown)
                }
            }

            /// Attempt to get a known specific vendor in the relevant vendor category list
            /// directly from an owned [`SString`]
            #[inline]
            pub fn try_from_vendor_sstring(v: $crate::commontypes::SString) -> Result<Self, VendorIDError> {
                match Self::maybe_from_known_vendor_str(v.as_str()) {
                    Some(s) => Ok(s),
                    None => VendorID::try_from(v).map(Self::Unknown)
                }
            }

            #[inline]
            pub fn try_from_vendor_string(v: String) -> Result<Self, VendorIDError> {
                match Self::maybe_from_known_vendor_str(v.as_str()) {
                    Some(s) => Ok(s),
                    None => VendorID::try_from(v).map(Self::Unknown)
                }
            }

            /// Attempt to get a known specific vendor in the relevant vendor category list based
            /// on the given vendor ID.
            #[inline]
            pub fn from_vendor(v: VendorID) -> Self {
                match v.as_str() {
                    $($($vendor_name => Self::$variant_name,)*)*
                    _ => Self::Unknown(v)
                }
            }

            /// Attempt to get a known specific vendor in the relevant vendor category list. If it
            /// is not a known vendor, then instead of using the [`Self::Unknown`] variant, return
            /// a [`ControlFlow::Continue`], else return a [`ControlFlow::Break`] with ourselves.
            #[inline]
            pub fn from_known_vendor(v: VendorID) -> ControlFlow<Self, VendorID> {
                Self::from_vendor(v).continue_with_unknown_vendor()
            }

            /// re-generate a raw vendor id
            #[inline]
            pub fn into_vendor(self) -> VendorID {
                match self {
                    $($(Self::$variant_name =>
                            <VendorID as core::str::FromStr>::from_str($vendor_name)
                            .expect("fixed, known vendor string should be valid"),
                    )*)*
                    Self::Unknown(v) => v
                }
            }

            /// Get the string representation of this variant
            #[inline]
            pub fn as_str(&self) -> &str {
                match self {
                    $($(Self::$variant_name => $vendor_name,)*)*
                    Self::Unknown(v) => v.as_str()
                }
            }

            /// Get the "likely" package managers for this vendor, if any, in priority order.
            pub fn likely_package_managers(&self) -> &'static [PM] {
                match self {
                    $($(| Self::$variant_name)* => &[$($pkg_manager),*],)*
                    Self::Unknown(_) => &[]
                }
            }

            /// Continue if the vendor ID is unknown, else break.
            #[inline]
            pub fn continue_with_unknown_vendor(self) -> ControlFlow<Self, VendorID> {
                match self {
                    Self::Unknown(v) => ControlFlow::Continue(v),
                    v => ControlFlow::Break(v)
                }
            }
        }

        // implement display using the standardised string representation
        impl Display for $enum_name {
            #[inline]
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                f.write_str(self.as_str())
            }
        }

        // Implement `FromStr` and `TryFrom<&str>` and `TryFrom<SString>` and similar.
        impl TryFrom<&str> for $enum_name {
            type Error = VendorIDError;

            #[inline]
            fn try_from(value: &str) -> Result<Self, Self::Error> {
                Self::try_from_vendor_str(value)
            }
        }

        impl TryFrom<SString> for $enum_name {
            type Error = VendorIDError;

            #[inline]
            fn try_from(value: SString) -> Result<Self, Self::Error> {
                Self::try_from_vendor_sstring(value)
            }
        }


        impl TryFrom<String> for $enum_name {
            type Error = VendorIDError;

            #[inline]
            fn try_from(value: String) -> Result<Self, Self::Error> {
                Self::try_from_vendor_string(value)
            }
        }

        impl FromStr for $enum_name {
            type Err = VendorIDError;

            #[inline]
            fn from_str(s: &str) -> Result<Self, Self::Err> {
                Self::try_from(s)
            }
        }

        impl AsRef<str> for $enum_name {
            #[inline]
            fn as_ref(&self) -> &str {
                self.as_str()
            }
        }
    };
}

vendor_package_managers! {
    /// General Unix Vendor forms (mostly linux stuff).
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Default, Hash)]
    pub enum UnixVendor {
        {
            /// Generic "linux" vendor (i.e. indicating that a system is not any specific flavour).
            #[default]
            GenericLinux = "linux"
        } => [],
        {
            Ubuntu = "ubuntu",
            Debian = "debian",
            LinuxMint = "linuxmint",
            Pop = "pop",
            Deepin = "deepin",
            Elementary = "elementary",
            Kali = "kali",
            Raspbian = "raspbian",
            Aosc = "aosc",
            Zorin = "zorin",
            Antix = "antix",
            Devuan = "devuan",
            Bodhi = "bodhi",
            Lxle = "lxle",
            Sparky = "sparky"
        } => [PM::Apt],
        {
            Fedora = "fedora",
            Redhat = "redhat",
            RedhatEnterpiseLinux = "rhel",
            AmazonLinux = "amzn",
            Ol = "ol",
            AlmaLinux = "almalinux",
            RockyLinux = "rocky",
            Oubes = "oubes",
            CentOS = "centos",
            Qubes = "qubes",
            EuroLinux = "eurolinux"
        } => [PM::Dnf, PM::Yum],
        {
            ArchLinux = "arch",
            Manjaro = "manjaro",
            EndeavourOS = "endeavouros",
            ArcoLinux = "arcolinux",
            Garuda = "garuda",
            Antergos = "antergos",
            Kaos = "kaos"
        } => [PM::Pacman],
        {
            AlpineLinux = "alpine",
            PostmarketOS = "postmarket"
        } => [PM::Apk],
        {
            OpenSuse = "opensuse",
            OpenSuseLeap = "opensuse-leap",
            OpenSuseTumbleweed = "opensuse-tumbleweed"
        } => [PM::Zypper],
        { Nix = "nixos" } => [PM::NixEnv],
        { Gentoo = "gentoo", Funtoo = "funtoo" } => [PM::Emerge],
        { VoidLinux = "void" } => [PM::Xbps],
        { Mageia = "mageia" } => [PM::Urpm],
        { Slackware = "slackware" } => [PM::SlackPkg],
        { Solus = "solus" } => [PM::Eopkg],
        { OpenWrt = "openwrt" } => [PM::Opkg],
        { NuTyx = "nutyx" } => [PM::Cards],
        { Crux = "crux" } => [PM::PrtGet],
        { FreeBsd = "freebsd", GhostBsd = "ghostbsd" } => [PM::Pkg],
        { AndroidTermux = "android" } => [PM::PkgTermux],
        { MSys2 = "windows-msys2" } => [PM::Pacman]
    }
}

vendor_package_managers! {
    /// Haiku OS stuff
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Default, Hash)]
    pub enum HaikuVendor {
        { #[default] Haiku = "haiku" } => [PM::Pkgman]
    }
}

vendor_package_managers! {
    /// Windows vendor. Basically, this is just windows and unknown. MSYS2 is in the [UnixVendor]
    /// part.
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Default, Hash)]
    pub enum WindowsVendor {
        { #[default] Windows = "windows" } => [PM::Scoop, PM::Choco, PM::Winget]
    }
}

vendor_package_managers! {
    /// Apple vendors. Basically, this is just macos and unknown.
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Default, Hash)]
    pub enum AppleVendor {
        { #[default] MacOs = "macos" } => [PM::Brew, PM::Port]
    }
}

#[cfg(test)]
mod test_vendor_name_validity {
    use super::{AppleVendor, HaikuVendor, UnixVendor, WindowsVendor};

    #[test]
    fn valid_known_vendor_names() {
        UnixVendor::ensure_vendor_names_are_ok();
        HaikuVendor::ensure_vendor_names_are_ok();
        AppleVendor::ensure_vendor_names_are_ok();
        WindowsVendor::ensure_vendor_names_are_ok();
    }
}

/// All known vendor IDs, categorised.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Hash)]
#[cfg_attr(
    feature = "serde",
    derive(serde_with::SerializeDisplay, serde_with::DeserializeFromStr)
)]
pub enum Vendor {
    Unix(UnixVendor),
    Haiku(HaikuVendor),
    Windows(WindowsVendor),
    Apple(AppleVendor),
}

impl Vendor {
    /// Retrieve a [`Vendor`] from a string, but only if it's a known/fixed string.
    ///
    /// This is mostly useful for avoiding allocations internally - many types of vendor IDs can be
    /// worked with even for unknown strings (especially unixes).
    #[inline]
    pub fn maybe_from_known_vendor_str(s: &str) -> Option<Self> {
        None.or_else(|| UnixVendor::maybe_from_known_vendor_str(s).map(Self::Unix))
            .or_else(|| HaikuVendor::maybe_from_known_vendor_str(s).map(Self::Haiku))
            .or_else(|| WindowsVendor::maybe_from_known_vendor_str(s).map(Self::Windows))
            .or_else(|| AppleVendor::maybe_from_known_vendor_str(s).map(Self::Apple))
    }

    /// Try to match on something string-convertible that a [`VendorID`] can be constructed from.
    /// If the associated string is not known, but it's still a valid [`VendorID`], then the
    /// unknown vendor ID is placed in the category most appropriate for the target platform:
    /// * `android` - [`Vendor::Unix`]
    /// * `windows` - [`Vendor::Windows`]
    /// * `apple` - [`Vendor::Apple`]
    /// * `haiku` - [`Vendor::Haiku`]
    /// * other - [`Vendor::Unix`]
    #[inline]
    pub fn try_from_vendor_convertible<T: AsRef<str>>(
        v: T,
    ) -> Result<Self, <VendorID as TryFrom<T>>::Error>
    where
        VendorID: TryFrom<T>,
    {
        match Self::maybe_from_known_vendor_str(v.as_ref()) {
            Some(v) => Ok(v),
            None => VendorID::try_from(v).map(unknown_default_category()),
        }
    }

    /// Attempt to get a [`Vendor`] from a known vendor string. This will automatically pick a
    /// variant for a known vendor, and if unknown, will use a category most appropriate for an
    /// unknown vendor given the target platform of the binary.
    pub fn try_from_vendor_str(v: &str) -> Result<Self, VendorIDError> {
        Self::try_from_vendor_convertible(v)
    }

    /// Attempt to get a [`Vendor`] from a known vendor string. This will automatically pick a
    /// variant for a known vendor, and if unknown, will use a category most appropriate for an
    /// unknown vendor given the target platform of the binary.
    pub fn try_from_vendor_sstring(v: SString) -> Result<Self, VendorIDError> {
        Self::try_from_vendor_convertible(v)
    }

    /// Attempt to get a [`Vendor`] from a known vendor string. This will automatically pick a
    /// variant for a known vendor, and if unknown, will use a category most appropriate for an
    /// unknown vendor given the target platform of the binary.
    pub fn try_from_vendor_string(v: String) -> Result<Self, VendorIDError> {
        Self::try_from_vendor_convertible(v)
    }

    /// Get a [`Vendor`] from a known vendor string. This will automatically pick a variant for a
    /// known vendor, and if unknown, will use a category most appropriate for an unknown vendor
    /// given the target platform of the binary.
    pub fn from_vendor(v: VendorID) -> Self {
        Self::maybe_from_known_vendor_str(v.as_str())
            .unwrap_or_else(|| unknown_default_category()(v))
    }

    /// Get the raw [`VendorID`] for this vendor
    #[inline]
    pub fn into_inner_vendor(self) -> VendorID {
        match self {
            Vendor::Unix(v) => v.into_vendor(),
            Vendor::Haiku(v) => v.into_vendor(),
            Vendor::Windows(v) => v.into_vendor(),
            Vendor::Apple(v) => v.into_vendor(),
        }
    }

    /// Get the string representation for this vendor.
    ///
    /// Note that [`Vendor::eq`] is not necessarily fully equivalent to [`str::eq`]. In particular,
    /// an unknown vendor in different vendor categories will have the same string representation.
    #[inline]
    pub fn as_str(&self) -> &str {
        match self {
            Vendor::Unix(v) => v.as_str(),
            Vendor::Haiku(v) => v.as_str(),
            Vendor::Windows(v) => v.as_str(),
            Vendor::Apple(v) => v.as_str(),
        }
    }
}

impl From<UnixVendor> for Vendor {
    #[inline]
    fn from(value: UnixVendor) -> Self {
        Vendor::Unix(value)
    }
}

impl From<HaikuVendor> for Vendor {
    #[inline]
    fn from(value: HaikuVendor) -> Self {
        Vendor::Haiku(value)
    }
}

impl From<WindowsVendor> for Vendor {
    #[inline]
    fn from(value: WindowsVendor) -> Self {
        Vendor::Windows(value)
    }
}

impl From<AppleVendor> for Vendor {
    #[inline]
    fn from(value: AppleVendor) -> Self {
        Vendor::Apple(value)
    }
}

macro_rules! impl_vendor_tryfrom {
    ($($ty:ty);*) => {$(
        impl TryFrom<$ty> for Vendor {
            type Error = VendorIDError;

            #[inline]
            fn try_from(v: $ty) -> Result<Vendor, Self::Error> {
                Vendor::try_from_vendor_convertible(v)
            }
        }
    )*};
}

impl_vendor_tryfrom! {&str; String; SString}

impl From<VendorID> for Vendor {
    #[inline]
    fn from(value: VendorID) -> Self {
        Vendor::from_vendor(value)
    }
}

impl Display for Vendor {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

impl FromStr for Vendor {
    type Err = VendorIDError;

    #[inline]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::try_from(s)
    }
}

/// Vendor category in which to put an unknown vendor ID
///
/// This is based on the target platform
fn unknown_default_category() -> impl FnOnce(VendorID) -> Vendor {
    #[cfg(target_os = "haiku")]
    {
        |v| HaikuVendor::Unknown(v).into()
    }
    #[cfg(target_os = "macos")]
    {
        |v| AppleVendor::Unknown(v).into()
    }
    #[cfg(target_os = "android")]
    {
        |v| UnixVendor::Unknown(v).into()
    }
    #[cfg(target_os = "windows")]
    {
        |v| WindowsVendor::Unknown(v).into()
    }
    #[cfg(not(any(
        target_os = "haiku",
        target_os = "macos",
        target_os = "android",
        target_os = "windows"
    )))]
    {
        |v| UnixVendor::Unknown(v).into()
    }
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
