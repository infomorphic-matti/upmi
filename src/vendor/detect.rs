//! Automatic or semi-automatic detection of vendor.
//!
//! Much of this uses the code from [upt][upt-os-detection]
//!
//! [upt-os-detection]: https://github.com/sigoden/upt/blob/main/src/utils.rs

use std::path::Path;

use super::known_ids::Vendor;

#[cfg(target_os = "haiku")]
fn detect_os_internal(_root_path: &Path) -> Result<Option<Vendor>, std::io::Error> {
    use super::known_ids::HaikuVendor;

    Ok(Some(HaikuVendor::Haiku.into()))
}

#[cfg(target_os = "macos")]
fn detect_os_internal(_root_path: &Path) -> Result<Option<Vendor>, std::io::Error> {
    use super::known_ids::AppleVendor;

    Ok(Some(AppleVendor::MacOs.into()))
}

#[cfg(target_os = "android")]
fn detect_os_internal(_root_path: &Path) -> Result<Option<Vendor>, std::io::Error> {
    use super::known_ids::UnixVendor;

    Ok(Some(UnixVendor::AndroidTermux.into()))
}

#[cfg(target_os = "windows")]
fn detect_os_internal(_root_path: &Path) -> Result<Option<Vendor>, std::io::Error> {
    use super::known_ids::{UnixVendor, WindowsVendor};
    let is_msys2 = std::env::var_os("MSYSTEM").is_some();
    if is_msys2 {
        Ok(Some(UnixVendor::MSys2.into()))
    } else {
        Ok(Some(WindowsVendor::Windows.into()))
    }
}

#[cfg(not(any(
    target_os = "windows",
    target_os = "macos",
    target_os = "android",
    target_os = "haiku"
)))]
fn detect_os_internal(root_path: &Path) -> Result<Option<Vendor>, std::io::Error> {
    use std::io::{BufRead, BufReader};

    use chain_trans::Trans;

    use crate::vendor::{known_ids::UnixVendor, VendorID};

    let os_release_path = {
        let mut root = root_path.to_owned();
        root.push("etc/os-release");
        root
    };
    let os_release_file = std::fs::File::open(os_release_path)?.trans(BufReader::new);
    for line in os_release_file.lines() {
        let line = line?;
        // Get each line
        let Some((os_release_key, os_release_rest)) = line.split_once('=') else {
            continue;
        };
        // strip whitespace and check the value
        let ("ID", os_release_vendor_id) = (os_release_key.trim(), os_release_rest.trim()) else {
            continue;
        };
        // parse the unixy vendor amd if it's valid, return (else continue)
        if let Ok(vendor) = os_release_vendor_id.parse::<VendorID>() {
            return Ok(Some(UnixVendor::from_vendor(vendor).into()));
        } else {
            continue;
        }
    }
    Ok(None)
}

/// Attempt to detect the operating system
///
/// Optionally lets you provide a root path to look in. This (currently) only does anything
/// on unix-y targets that use the `/etc/os-release` standard. If the vendor id in that is
/// not a valid vendor id, then [`None`] is returned (this is also the case if there is no vendor id info).
pub fn detect_os(target_root_path: Option<&Path>) -> Result<Option<Vendor>, std::io::Error> {
    let root_path = target_root_path.unwrap_or("/".as_ref());
    detect_os_internal(root_path)
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
