//! Vendor category types holding information on specific instances. You probably want
//! [`InstanceInformation`] or [`UnixInstanceInformation`] (these are the two with the most
//! powerful extra capabilities).
//!
//! These themselves use a list of known vendor ids internally, for package managers to access.

use super::known_ids::{AppleVendor, HaikuVendor, UnixVendor, Vendor, WindowsVendor};
use chain_trans::Trans as _;
use std::path::{Path, PathBuf};

/// Generic instance information methods, applicable to all Instance Information structures.
pub trait GenericInstanceInformation {
    /// Get the vendor of this system instance.
    fn vendor(&self) -> Vendor;

    /// Get if this instance is the currently-running system.
    ///
    /// Certain package manager searchers only work when the vendor instance being-searched-in is
    /// the "current" system (e.g. if they use the `PATH` environment variable). This function
    /// documents whether or not something is the "current" running system (it's somewhat of a
    /// heuristic, but it should work).
    fn is_current_running_system(&self) -> bool;
}

/// Instance information that has information on the root location of the system upon which package
/// management should be applied (NOT the executing system). Right now, only implemented by
/// [`UnixInstanceInformation`], but this can be used for any package manager that needs a specific
/// root location to look in for some operation or another.
pub trait WithRootInstanceInformation {
    /// Get the root location of the system upon which package management should be applied.
    fn target_system_root(&self) -> &Path;
}

/// Unix-y vendor - typically with an `os-release` file
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct UnixInstanceInformation {
    /// Vendor identifier.
    ///
    /// If unsure, use plain `linux`
    instance_vendor: UnixVendor,
    /// Root of the filesystem of the vendor. If just messing with your own system, probably want
    /// this to be `/`.
    ///
    /// This refers to the location upon which package management should be applied, not
    /// necessarily to binary location.
    target_root_path: PathBuf,
}

impl UnixInstanceInformation {
    /// Create an instance of a unix system with the given vendor at the root directory of the
    /// filesystem ("/")
    pub fn with_vendor(vendor: UnixVendor) -> Self {
        Self {
            instance_vendor: vendor,
            target_root_path: PathBuf::from("/"),
        }
    }

    /// Create an instance of a unix system with the given vendor and given root path upon which
    /// package management should be applied.
    pub const fn with_vendor_and_root(vendor: UnixVendor, root: PathBuf) -> Self {
        Self {
            instance_vendor: vendor,
            target_root_path: root,
        }
    }

    /// Get the reported vendor of this unix system
    #[inline]
    pub const fn get_vendor(&self) -> &UnixVendor {
        &self.instance_vendor
    }

    /// Get the root directory of the target unix system
    #[inline]
    pub fn get_system_root(&self) -> &Path {
        &self.target_root_path
    }
}

impl GenericInstanceInformation for UnixInstanceInformation {
    #[inline]
    fn vendor(&self) -> Vendor {
        self.get_vendor().clone().into()
    }

    #[inline]
    fn is_current_running_system(&self) -> bool {
        self.target_root_path.has_root()
    }
}

impl WithRootInstanceInformation for UnixInstanceInformation {
    #[inline]
    fn target_system_root(&self) -> &Path {
        &self.target_root_path
    }
}

/// Windows-like instance info. Basically has nothing particularly interesting in it.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct WindowsInstanceInformation {
    instance_vendor: WindowsVendor,
}

impl WindowsInstanceInformation {
    pub const fn with_vendor(vendor: WindowsVendor) -> Self {
        Self {
            instance_vendor: vendor,
        }
    }

    #[inline]
    pub const fn get_vendor(&self) -> &WindowsVendor {
        &self.instance_vendor
    }
}

impl GenericInstanceInformation for WindowsInstanceInformation {
    #[inline]
    fn vendor(&self) -> Vendor {
        self.get_vendor().clone().into()
    }

    // Unix is the only platform we're likely to be able to modify other instances from anyway.
    #[inline]
    fn is_current_running_system(&self) -> bool {
        true
    }
}

/// Haiku-like instance info. Basically has nothing particularly interesting in it atm.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct HaikuInstanceInformation {
    instance_vendor: HaikuVendor,
}

impl HaikuInstanceInformation {
    pub fn with_vendor(vendor: HaikuVendor) -> Self {
        Self {
            instance_vendor: vendor,
        }
    }

    #[inline]
    pub const fn get_vendor(&self) -> &HaikuVendor {
        &self.instance_vendor
    }
}

impl GenericInstanceInformation for HaikuInstanceInformation {
    #[inline]
    fn vendor(&self) -> Vendor {
        self.get_vendor().clone().into()
    }

    // Unix is the only platform we're likely to be able to modify other instances from anywway
    fn is_current_running_system(&self) -> bool {
        true
    }
}

/// Apple-like instance info. Basically has nothing particularly interesting in it atm.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct AppleInstanceInformation {
    instance_vendor: AppleVendor,
}

impl AppleInstanceInformation {
    pub fn with_vendor(vendor: AppleVendor) -> Self {
        Self {
            instance_vendor: vendor,
        }
    }

    #[inline]
    pub const fn get_vendor(&self) -> &AppleVendor {
        &self.instance_vendor
    }
}

impl GenericInstanceInformation for AppleInstanceInformation {
    #[inline]
    fn vendor(&self) -> Vendor {
        self.get_vendor().clone().into()
    }

    // Unix is the only platform we're likely to be able to modify other instances from anywway
    #[inline]
    fn is_current_running_system(&self) -> bool {
        true
    }
}

/// Instance info for any one of the specific vendor types.
///
/// This is where a lot of useful stuff for detection comes in.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum InstanceInformation {
    Unix(UnixInstanceInformation),
    Windows(WindowsInstanceInformation),
    Apple(AppleInstanceInformation),
    Haiku(HaikuInstanceInformation),
}

impl InstanceInformation {
    /// Create instance information from a directly created [Vendor], using any "default" root path
    /// location if it's applicable.
    ///
    /// If you want to detect this automatically, see [InstanceInformation::detect_os].
    pub fn from_vendor(vendor: Vendor) -> Self {
        match vendor {
            Vendor::Unix(vendor) => UnixInstanceInformation::with_vendor(vendor).into(),
            Vendor::Haiku(vendor) => HaikuInstanceInformation::with_vendor(vendor).into(),
            Vendor::Windows(vendor) => WindowsInstanceInformation::with_vendor(vendor).into(),
            Vendor::Apple(vendor) => AppleInstanceInformation::with_vendor(vendor).into(),
        }
    }

    /// Create instance information from a directly created [Vendor], with provided target root
    /// path information if applicable.
    ///
    /// If you want to detect things automatically, see [InstanceInformation::detect_os_in].
    pub fn from_vendor_and_root(vendor: Vendor, target_root: PathBuf) -> Self {
        match vendor {
            Vendor::Unix(v) => UnixInstanceInformation::with_vendor_and_root(v, target_root).into(),
            Vendor::Haiku(v) => HaikuInstanceInformation::with_vendor(v).into(),
            Vendor::Windows(v) => WindowsInstanceInformation::with_vendor(v).into(),
            Vendor::Apple(v) => AppleInstanceInformation::with_vendor(v).into(),
        }
    }

    #[cfg(feature = "identify-vendor")]
    /// Attempt to detect the operating system vendor information, with no special root path
    /// information. To provide root path information, see [`InstanceInformation::detect_os_in`].
    ///
    /// If it can't be detected, you could get raw vendor IDs, or use platform defaults.
    pub fn detect_os() -> Result<Option<Self>, std::io::Error> {
        Self::detect_os_in(None)
    }

    #[cfg(feature = "identify-vendor")]
    /// Attempt to detect the operating system, while optionally providing a target root directory
    /// to look in. Note that on non-unix-y platforms, the root directory parameter may not get used.
    pub fn detect_os_in(
        target_system_root: Option<PathBuf>,
    ) -> Result<Option<Self>, std::io::Error> {
        use super::detect;

        detect::detect_os(target_system_root.as_deref())?
            .map(|vendor| match target_system_root {
                Some(system_root) => Self::from_vendor_and_root(vendor, system_root),
                None => Self::from_vendor(vendor),
            })
            .trans(Ok)
    }
}

impl GenericInstanceInformation for InstanceInformation {
    fn vendor(&self) -> Vendor {
        match self {
            InstanceInformation::Unix(instance) => instance.vendor(),
            InstanceInformation::Windows(instance) => instance.vendor(),
            InstanceInformation::Apple(instance) => instance.vendor(),
            InstanceInformation::Haiku(instance) => instance.vendor(),
        }
    }

    fn is_current_running_system(&self) -> bool {
        match self {
            InstanceInformation::Unix(instance) => instance.is_current_running_system(),
            InstanceInformation::Windows(instance) => instance.is_current_running_system(),
            InstanceInformation::Apple(instance) => instance.is_current_running_system(),
            InstanceInformation::Haiku(instance) => instance.is_current_running_system(),
        }
    }
}

macro_rules! impl_instance_info_from {
    ($variant:ident, $contains:ident) => {
        impl From<$contains> for InstanceInformation {
            #[inline]
            fn from(value: $contains) -> InstanceInformation {
                InstanceInformation::$variant(value)
            }
        }
    };
}

impl_instance_info_from! {Unix, UnixInstanceInformation}
impl_instance_info_from! {Windows, WindowsInstanceInformation}
impl_instance_info_from! {Apple, AppleInstanceInformation}
impl_instance_info_from! {Haiku, HaikuInstanceInformation}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
