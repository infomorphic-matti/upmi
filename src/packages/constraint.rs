//! Constraint operations on packages.
//!
//! ## Understanding Constraints
//! Many package managers allow specifying specific versions of packages, or constraining the
//! versions of packages (below or above), similar (but usually slightly less powerful) to
//! how `cargo` lets you specify dependencies to a package above, below, or between certain
//! versions.
//!
//! This is fairly different between different package managers. To get around this, we provide
//! abstracted constraints (in the form of the [PackageVersionConstraint] enum), which embeds the
//! relevant version data.

use core::{fmt::Display, iter};

use crate::packages::with_packages::Unvalidated;

use super::with_packages::{self, UnvalidatedPackageInfo, Validated};

/// Boundary for range-like [`PackageVersionConstraint`] values.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum VersionBoundary<Version = super::PackageVersion> {
    Inclusive(Version),
    Exclusive(Version),
}

impl<Version: Display> Display for VersionBoundary<Version> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            VersionBoundary::Inclusive(v) => f.write_fmt(format_args!("[{v}(inclusive)]")),
            VersionBoundary::Exclusive(v) => f.write_fmt(format_args!("[{v}(exclusive)]")),
        }
    }
}

impl<V> UnvalidatedPackageInfo<V> for VersionBoundary<V> {
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = with_packages::Unvalidated<'s, V>>
    where
        V: 's,
    {
        match self {
            VersionBoundary::Inclusive(v) => with_packages::Unvalidated::version(v).once(),
            VersionBoundary::Exclusive(v) => with_packages::Unvalidated::version(v).once(),
        }
    }

    type Validated = Validated<Self>
    where
        Self: Sized;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
        Self::Validated: Sized,
    {
        Validated::new(self)
    }
}

/// Enum specifying all possible package constraints (including no constraint)
///
/// When implementing functions that handle constraints, package managers should always implement
/// functionality for [`PackageVersionConstraint::Unconstrained`], and almost always should
/// implement functionality for [`PackageVersionConstraint::Exact`]. Functionality for these - both
/// when installing a package and when reading information about packages - usually doesn't need
/// things like the package-manager-specific comparison functionality.
///
/// Other operations may or may not be supported - unsupported operations should raise the
/// [PackageVersionConstraint] error. This enum also implements [Display], which provides a
/// relatively nice, readable listing of a constraint (or empty for unconstrained), but may not
/// necessarily fit the format your package manager wants. To get the "standard" comparison symbols
/// (for operations which take exactly one argument) - like '=', '<', '<=', etc., use
/// [`PackageVersionConstraint::comparison_name`], then use [`PackageVersionConstraintName::symbol`].
#[non_exhaustive]
#[derive(Debug, Clone, Copy)]
pub enum PackageVersionConstraint<Version = super::PackageVersion> {
    /// No constraint on a package version. This typically ends up referring to the "latest
    /// available" version of a package.
    ///
    /// All package managers must implement for this.
    Unconstrained,
    /// Specify an exact version of a package.
    ///
    /// Almost all package managers will be able to do this too.
    Exact(Version),
    /// Specify a version of a package that is < the given version.
    LessThan(Version),
    /// Specify a version of a package that is <= the given version
    LessThanOrEqual(Version),
    /// Specify a version of a package that is > the given version.
    GreaterThan(Version),
    /// Specify a version of a package that is >= the given version.
    GreaterThanOrEqual(Version),
    /// Specify a version of a package that is between two versions
    ///
    /// Note - we cannot validate that the `start` version is lower than the `end` version
    ///
    /// If an implementation can detect this and it causes a detectable error, then they should use
    /// [PackageVersionConstraintError::Invalid]
    Between {
        start: VersionBoundary<Version>,
        end: VersionBoundary<Version>,
    },
    /// Specify a version of a package that is either below the first version, or above the second
    /// version.
    ///
    /// Note - we cannot validate that the `start` version is lower than the `end` version
    ///
    /// If an implementation can detect this and it causes a detectable error, then they should use
    /// [PackageVersionConstraintError::Invalid]
    Outside {
        start: VersionBoundary<Version>,
        end: VersionBoundary<Version>,
    },
}
impl<Version: Display> Display for PackageVersionConstraint<Version> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Utility structs/traits for rendering the version boundaries.
        struct Below<'s, V: 's>(pub &'s VersionBoundary<V>);
        struct Above<'s, V: 's>(pub &'s VersionBoundary<V>);
        impl<'s, V: Display + 's> Display for Below<'s, V> {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                match self.0 {
                    VersionBoundary::Inclusive(v) => write!(f, "<={v}"),
                    VersionBoundary::Exclusive(v) => write!(f, "<{v}"),
                }
            }
        }

        impl<'s, V: Display + 's> Display for Above<'s, V> {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                match self.0 {
                    VersionBoundary::Inclusive(v) => write!(f, ">={v}"),
                    VersionBoundary::Exclusive(v) => write!(f, ">{v}"),
                }
            }
        }

        match self {
            PackageVersionConstraint::Unconstrained => write!(f, ""),
            PackageVersionConstraint::Exact(v) => write!(f, "={v}"),
            PackageVersionConstraint::LessThan(v) => write!(f, "<{v}"),
            PackageVersionConstraint::LessThanOrEqual(v) => write!(f, "<={v}"),
            PackageVersionConstraint::GreaterThan(v) => write!(f, ">{v}"),
            PackageVersionConstraint::GreaterThanOrEqual(v) => write!(f, ">={v}"),
            PackageVersionConstraint::Between { start, end } => {
                write!(f, "{},{}", Above(start), Below(end))
            }
            PackageVersionConstraint::Outside { start, end } => {
                write!(f, "{},{}", Below(start), Above(end))
            }
        }
    }
}

impl<Version> UnvalidatedPackageInfo<Version> for PackageVersionConstraint<Version> {
    type Validated = Validated<Self>
    where
        Self: Sized;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
        Self::Validated: Sized,
    {
        Validated::new(self)
    }

    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = with_packages::Unvalidated<'s, Version>> + 's
    where
        Version: 's,
    {
        let no_version_iter = || either::Left(iter::empty());
        let single_version_iter =
            |ver| either::Right(either::Left(with_packages::unvalidated_version(ver).once()));

        // Workaround for issues in closures.
        fn two_version_iter_2<'s, Version: 's>(
            v0: &'s VersionBoundary<Version>,
            v1: &'s VersionBoundary<Version>,
        ) -> impl Iterator<Item = Unvalidated<'s, Version>> + 's {
            v0.unvalidated_package_names_and_versions()
                .chain(v1.unvalidated_package_names_and_versions())
        }
        let two_version_iter = |v0, v1| either::Right(either::Right(two_version_iter_2(v0, v1)));
        match self {
            PackageVersionConstraint::Unconstrained => no_version_iter(),
            PackageVersionConstraint::Exact(v)
            | PackageVersionConstraint::LessThan(v)
            | PackageVersionConstraint::LessThanOrEqual(v)
            | PackageVersionConstraint::GreaterThan(v)
            | PackageVersionConstraint::GreaterThanOrEqual(v) => single_version_iter(v),
            PackageVersionConstraint::Between { start, end }
            | PackageVersionConstraint::Outside { start, end } => two_version_iter(start, end),
        }
    }
}

impl<Version> PackageVersionConstraint<Version> {
    #[inline]
    pub const fn comparison_name(&self) -> PackageVersionConstraintName {
        use PackageVersionConstraint as S;
        use PackageVersionConstraintName as N;
        match self {
            S::Unconstrained => N::Unconstrained,
            S::Exact(_) => N::Exact,
            S::LessThan(_) => N::LessThan,
            S::LessThanOrEqual(_) => N::LessThanOrEqual,
            S::GreaterThan(_) => N::GreaterThan,
            S::GreaterThanOrEqual(_) => N::GreaterThanOrEqual,
            S::Between { .. } => N::Between,
            S::Outside { .. } => N::Outside,
        }
    }

    /// Returns true if this is unconstrained, false otherwise.
    #[inline]
    pub const fn unconstrained(&self) -> bool {
        matches!(self, Self::Unconstrained)
    }

    /// Returns true if the [`Display`] format of this operation is of the form
    /// `{comparison-operator}{version-string}` (for instance, `=1.0.2-rc1`, `<=0.3`, `>3:1.0`).
    /// This generally is true for the same type of constraints that return [Some] for
    /// [PackageVersionConstraintName::symbol].
    ///
    /// This function is very useful to save significant work - many package managers use that kind
    /// of formatting structure, and this function lets you just reuse the [`Display`]
    /// implementation for [PackageVersionConstraint] in those cases.
    #[inline]
    pub const fn comparison_and_version_string(&self) -> bool {
        match self {
            PackageVersionConstraint::Unconstrained => false,
            PackageVersionConstraint::Exact(_) => true,
            PackageVersionConstraint::LessThan(_) => true,
            PackageVersionConstraint::LessThanOrEqual(_) => true,
            PackageVersionConstraint::GreaterThan(_) => true,
            PackageVersionConstraint::GreaterThanOrEqual(_) => true,
            PackageVersionConstraint::Between { .. } => false,
            PackageVersionConstraint::Outside { .. } => false,
        }
    }

    /// Create an error indicating this operation is unsupported.
    ///
    /// See notes on [`PackageVersionConstraintError::Unsupported`] for when you should use this.
    #[inline]
    pub const fn unsupported(self) -> Result<(), PackageVersionConstraintError<Version>>
    where
        Version: Display,
    {
        Err(PackageVersionConstraintError::Unsupported(self))
    }

    /// Create an error indicating this operation is invalid.
    ///
    /// See notes on [`PackageVersionConstraintError::Invalid`]
    #[inline]
    pub const fn invalid(self) -> Result<(), PackageVersionConstraintError<Version>>
    where
        Version: Display,
    {
        Err(PackageVersionConstraintError::Invalid(self))
    }
}

/// Like [`PackageVersionConstraint`], but without attached version information
#[non_exhaustive]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum PackageVersionConstraintName {
    Unconstrained,
    Exact,
    LessThan,
    LessThanOrEqual,
    GreaterThan,
    GreaterThanOrEqual,
    Between,
    Outside,
}

impl PackageVersionConstraintName {
    /// Get the "standard" symbol for constraint types that have a single associated version.
    ///
    /// For example, "=", "<=", "<", ">", etc.
    ///
    /// This will return [None] for unconstraining "constraints", as well as constraints which have
    /// multiple version parameters such as [`PackageVersionConstraint::Between`], or constraints
    /// where there is no "standard" symbol.
    #[inline]
    pub const fn symbol(&self) -> Option<&'static str> {
        match self {
            PackageVersionConstraintName::Unconstrained => None,
            PackageVersionConstraintName::Exact => Some("="),
            PackageVersionConstraintName::LessThan => Some("<"),
            PackageVersionConstraintName::LessThanOrEqual => Some("<="),
            PackageVersionConstraintName::GreaterThan => Some(">"),
            PackageVersionConstraintName::GreaterThanOrEqual => Some(">="),
            PackageVersionConstraintName::Between => None,
            PackageVersionConstraintName::Outside => None,
        }
    }
}

#[derive(Debug, Clone, thiserror::Error)]
pub enum PackageVersionConstraintError<Version: Display = super::PackageVersion> {
    /// The package manager implementation does not support the requested version constraint type.
    ///
    /// Note - if the implementation *does* support the version constraint type, but the specific
    /// parameters to it were incorrect (e.g. there was a detectable error in the version string,
    /// there was an issue where one of the range-like constraints had start > end, etc.), then
    /// they should use [`PackageVersionConstraintError::Invalid`] instead.
    ///
    /// Note 2 - the [`VersionBoundary::Inclusive`]/[`VersionBoundary::Exclusive`] variants - used
    /// in the `Between` and `Outside` constraints - are considered part of the version constraint
    /// type, and not the version constraint parameters, for the purpose of errors.
    #[error("unsupported version constraint: {0}")]
    Unsupported(PackageVersionConstraint<Version>),
    /// The implementation supports the given type of version constraint, but an actual parameter
    /// to it was detectably invalid.
    #[error("invalid version constraint: {0}")]
    Invalid(PackageVersionConstraint<Version>),
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
