//! Module for types that contain [`PackageName`][`super::PackageName`] and package version
//! (typically [`PackageVersion`][`super::PackageVersion`]) information, for the purposes of
//! validation. The primary relevant trait is [`UnvalidatedPackageInfo`].

use core::{
    iter::{self, once},
    ops::Deref,
};
use std::{rc::Rc, sync::Arc};

use super::{PackageName, PackageVersion};

/// Some kind of package data that isn't validated yet.
#[derive(Debug)]
pub enum Unvalidated<'n, Version: 'n = PackageVersion> {
    PackageName(&'n PackageName),
    PackageVersion(&'n Version),
}

// Not #[derive] because clone/copy-ability is not dependent on the clone/copy-ability of the type
// params.
impl<'n, Version: 'n> Copy for Unvalidated<'n, Version> {}
impl<'n, Version: 'n> Clone for Unvalidated<'n, Version> {
    #[inline]
    fn clone(&self) -> Self {
        *self
    }
}

impl<'n, Version: 'n> Unvalidated<'n, Version> {
    /// Create an unvalidated version
    #[inline]
    pub const fn version(v: &'n Version) -> Self {
        Self::PackageVersion(v)
    }

    /// Create an unvalidated name
    #[inline]
    pub const fn name(n: &'n PackageName) -> Self {
        Self::PackageName(n)
    }

    /// Turn a single [`Unvalidated`] into an iterator over a single [`Unvalidated`]
    #[inline]
    pub fn once(self) -> impl Iterator<Item = Self> {
        once(self)
    }
}

/// Create an [`Unvalidated`] for the given package name
#[inline]
pub const fn unvalidated_name<'n, Version: 'n>(name: &'n PackageName) -> Unvalidated<'n, Version> {
    Unvalidated::name(name)
}

/// Create an [`Unvalidated`] for the given package version
#[inline]
pub const fn unvalidated_version<'n, Version: 'n>(
    version: &'n Version,
) -> Unvalidated<'n, Version> {
    Unvalidated::version(version)
}

/// Type that can contain package information for validation. Specifically, if a type knows that
/// it's contained package names and versions have been validated by a package manager already,
/// then it can implement this trait and provide an "empty list".
pub trait UnvalidatedPackageInfo<Version = PackageVersion> {
    /// Version of the type wrapped in [`Validated`]. This almost always should just be
    /// [`Validated<Self>`], but for [`Validated<Self>`] itself, this isn't the case (as well as
    /// for certain others like mutable refs where we want to just directly get `Validated<&T>`
    /// without going through `Validated<&mut T>`, even if the structure of `Validated` will
    /// prevent modifications as there's no way to get an `&mut` out of it).
    ///
    /// This actually works a lot like the `Pin` system.
    type Validated: UnvalidatedPackageInfo<Version>
    where
        Self: Sized;

    /// Make a version of this type that is marked validated by being wrapped in [`Validated`]
    ///
    /// # Safety
    /// The value passed is actually validated.
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
        Self::Validated: Sized;

    /// All package names and versions that haven't been validated yet, contained in this type.
    ///
    /// This is one function rather than split in two (via a custom enum), as it lets you iterate
    /// only once over collections containing both package names and package versions, or mapping
    /// package names *to* package versions (very common).
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, Version>> + 's
    where
        Version: 's;
}

/// Marker struct that indicates a type `T` has been validated. If `T` implements
/// [`UnvalidatedPackageInfo`], then this type also implements it, but produces an empty iterator
/// for [`UnvalidatedPackageInfo::unvalidated_package_names_and_versions`], as this type indicates
/// the values have already been validated inside `T`.
///
/// This will not let you get an `&mut T` from inside - you must either work through shared
/// references, or take the value out by-value.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
#[repr(transparent)]
pub struct Validated<T: ?Sized>(T);

impl<T: ?Sized> Validated<T> {
    /// Mark a given value as having been "validated" by a package manager's
    /// [`crate::managers::validating::Validating`] implementation.
    ///
    /// # Safety
    /// Ensure it's actually validated and can't be modified to be invalidated while the
    /// [`Validated`] structure exists. Rust lifetimes generally let you do this seamlessly.
    #[inline]
    pub const unsafe fn new(v: T) -> Self
    where
        T: Sized,
    {
        Self(v)
    }

    /// Extract the known-validated value.
    #[inline]
    pub fn into_inner(self) -> T
    where
        T: Sized,
    {
        self.0
    }

    #[inline]
    pub const fn as_inner(&self) -> &T {
        &self.0
    }

    /// Convert `Validated<T>` into `Validated<&T>`
    #[inline]
    pub const fn as_validated_inner(&self) -> Validated<&T> {
        Validated(&self.0)
    }

    /// Take a "wrapped" validated value and convert it into it's canonical validated form.
    ///
    /// Useful for when you've done transforms and want the canonical validated form.
    #[inline]
    pub fn into_validated<V>(self) -> T::Validated
    where
        T: UnvalidatedPackageInfo<V, Validated: Sized> + Sized,
    {
        // Safety: Because `self` is `Validate`, we know the contained value is validated.
        unsafe { T::make_validated(self.0) }
    }

    /// Map a validated component to a subcomponent.
    ///
    /// # Safety
    /// You must ensure that validating the top level component implies that the produced component
    /// is validated, and cannot be modified in-place via the validated handle (i.e. it has no
    /// interior mutability).
    #[inline]
    pub unsafe fn validated_project<O>(self, map: impl FnOnce(T) -> O) -> Validated<O>
    where
        T: Sized,
    {
        Validated::new(map(self.0))
    }
}

impl<T: ?Sized> AsRef<T> for Validated<T> {
    #[inline]
    fn as_ref(&self) -> &T {
        self.as_inner()
    }
}

impl<V, T: ?Sized + UnvalidatedPackageInfo<V>> UnvalidatedPackageInfo<V> for Validated<T> {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>> + 's
    where
        V: 's,
    {
        iter::empty()
    }

    type Validated = Self where Self: Sized;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
        Self::Validated: Sized,
    {
        self
    }
}

impl<V> UnvalidatedPackageInfo<V> for PackageName {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>> + 's
    where
        V: 's,
    {
        unvalidated_name(self).once()
    }

    type Validated = Validated<Self>;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
    {
        Validated::new(self)
    }
}

// Cannot do generically for all types because it would produce conflicting impls
impl UnvalidatedPackageInfo<PackageVersion> for PackageVersion {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, PackageVersion>>
    where
        PackageVersion: 's,
    {
        unvalidated_version(self).once()
    }

    type Validated = Validated<Self>;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
    {
        Validated::new(self)
    }
}

impl<V> UnvalidatedPackageInfo<V> for Unvalidated<'_, V> {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>>
    where
        V: 's,
    {
        self.once()
    }

    type Validated = Validated<Self>;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
    {
        Validated::new(self)
    }
}

// Blanket impls
impl<'a, V, T: UnvalidatedPackageInfo<V> + ?Sized + 'a> UnvalidatedPackageInfo<V> for &'a T {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>>
    where
        V: 's,
    {
        T::unvalidated_package_names_and_versions(self)
    }

    type Validated = Validated<Self>;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
    {
        Validated::new(self)
    }
}

impl<'a, V, T: UnvalidatedPackageInfo<V> + ?Sized + 'a> UnvalidatedPackageInfo<V> for &'a mut T {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>>
    where
        V: 's,
    {
        T::unvalidated_package_names_and_versions(self)
    }

    type Validated = Validated<&'a T>;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
    {
        Validated::new(&*self)
    }
}

macro_rules! impl_unvalidated_package_info_deref {
    ($($wrapper:ident),*) => {$(
        impl<V, T: UnvalidatedPackageInfo<V> + ?Sized> UnvalidatedPackageInfo<V> for $wrapper<T> {
            type Validated = Validated<Self>;

            #[inline]
            unsafe fn make_validated(self) -> Self::Validated
            where
                Self: Sized
            {
                Validated::new(self)
            }

            #[inline]
            fn unvalidated_package_names_and_versions<'s>(
                &'s self,
            ) -> impl Iterator<Item = Unvalidated<'s, V>>
            where
                V: 's,
            {
                self.deref().unvalidated_package_names_and_versions()
            }
        }
    )*};
}

impl_unvalidated_package_info_deref! {Box, Rc, Arc}

macro_rules! impl_unvalidated_package_info_tuples {
    ($($tuplety:ident),*) => {
        impl <V, $($tuplety: UnvalidatedPackageInfo<V>,)*> UnvalidatedPackageInfo<V> for ($($tuplety,)*) {
            type Validated = ($(<$tuplety as UnvalidatedPackageInfo<V>>::Validated,)*) where Self: Sized;

            // This is for the "null" case as well as to avoid duplicating names
            #[allow(non_snake_case)]
            #[allow(clippy::unused_unit)]
            #[inline]
            unsafe fn make_validated(self) -> Self::Validated
                where Self: Sized, Self::Validated: Sized
            {
                let ($($tuplety,)*) = self;
                $(let $tuplety = <$tuplety as UnvalidatedPackageInfo<V>>::make_validated($tuplety);)*
                ($($tuplety,)*)
            }

            // This is to avoid needing to provide separate type + variable names for the tuple
            // bits.
            #[allow(non_snake_case)]
            #[inline]
            fn unvalidated_package_names_and_versions<'s>(
                &'s self
            ) -> impl Iterator<Item = Unvalidated<'s, V>>
            where
                V: 's
            {
                let ($($tuplety,)*) = self;
                let iter = core::iter::empty();
                $(let iter = iter.chain($tuplety.unvalidated_package_names_and_versions());)*
                iter
            }
        }
    };
}

impl_unvalidated_package_info_tuples! {}
impl_unvalidated_package_info_tuples! {T0}
impl_unvalidated_package_info_tuples! {T0, T1}
impl_unvalidated_package_info_tuples! {T0, T1, T2}
impl_unvalidated_package_info_tuples! {T0, T1, T2, T3}
impl_unvalidated_package_info_tuples! {T0, T1, T2, T3, T4}
impl_unvalidated_package_info_tuples! {T0, T1, T2, T3, T4, T5}
impl_unvalidated_package_info_tuples! {T0, T1, T2, T3, T4, T5, T6}
impl_unvalidated_package_info_tuples! {T0, T1, T2, T3, T4, T5, T6, T7}

impl<V, T: UnvalidatedPackageInfo<V>> UnvalidatedPackageInfo<V> for Vec<T> {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>>
    where
        V: 's,
    {
        self.iter()
            .flat_map(T::unvalidated_package_names_and_versions)
    }

    type Validated = Validated<Self>
    where
        Self: Sized;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
        Self::Validated: Sized,
    {
        Validated::new(self)
    }
}

impl<V, T: UnvalidatedPackageInfo<V>> UnvalidatedPackageInfo<V> for [T] {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>>
    where
        V: 's,
    {
        self.iter()
            .flat_map(T::unvalidated_package_names_and_versions)
    }

    type Validated = Validated<Self>
    where
        Self: Sized;

    // Not actually possible? even for zero-sized T I think `[T]` is still unsized?
    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
        Self::Validated: Sized,
    {
        Validated::new(self)
    }
}

impl<V, T: UnvalidatedPackageInfo<V>, const N: usize> UnvalidatedPackageInfo<V> for [T; N] {
    #[inline]
    fn unvalidated_package_names_and_versions<'s>(
        &'s self,
    ) -> impl Iterator<Item = Unvalidated<'s, V>>
    where
        V: 's,
    {
        self.iter()
            .flat_map(T::unvalidated_package_names_and_versions)
    }

    type Validated = [T::Validated; N]
    where
        Self: Sized;

    #[inline]
    unsafe fn make_validated(self) -> Self::Validated
    where
        Self: Sized,
        Self::Validated: Sized,
    {
        // Safety:
        // The whole array is validated value by value, so we can make each element validated too.
        self.map(|v| unsafe { T::make_validated(v) })
    }
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
