//! Basic package version information as a string (or more accurately a smallstr).
//!
//! Package managers may provide more structured types for versions in future.

use core::{borrow::Borrow, fmt::Display, str::FromStr};

use crate::{commontypes::SString, utils::ntypeutils::SStringNewtype};

use super::validation;

/// Raw package version string.
///
/// This has much less structure than typically would be desired - for the reasons discussed in the
/// [parent module][`super`]. It does, however, have significant tools for validation, mirroring
/// those of [`super::PackageName`]
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct PackageVersion(SString);

impl Display for PackageVersion {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

impl PackageVersion {
    #[inline]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    #[inline]
    pub fn into_inner(self) -> SString {
        self.0
    }
}

impl From<SString> for PackageVersion {
    #[inline]
    fn from(value: SString) -> Self {
        PackageVersion(value)
    }
}

impl From<String> for PackageVersion {
    #[inline]
    fn from(value: String) -> Self {
        Self::from(SString::from_string(value))
    }
}

impl From<&str> for PackageVersion {
    #[inline]
    fn from(value: &str) -> Self {
        Self::from(SString::from_str(value))
    }
}

impl FromStr for PackageVersion {
    type Err = ();

    #[inline]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self::from(s))
    }
}

impl AsRef<str> for PackageVersion {
    #[inline]
    fn as_ref(&self) -> &str {
        self.0.as_str()
    }
}

impl Borrow<str> for PackageVersion {
    #[inline]
    fn borrow(&self) -> &str {
        self.as_ref()
    }
}

impl SStringNewtype for PackageVersion {
    #[inline]
    fn into_inner_sstring(self) -> SString {
        self.0
    }

    #[inline]
    fn from_sstring(value: SString) -> Self {
        Self(value)
    }
}

impl validation::StringDataProcessingInner for PackageVersion {
    type Error<CustomError> = PackageVersionError<CustomError, PackageVersion>;

    #[inline]
    fn make_banned_character_error<E>(&self, found_banned_char: char) -> Self::Error<E> {
        PackageVersionError::<E, Self>::ContainsBannedCharacter(self.clone(), found_banned_char)
    }

    #[inline]
    fn make_empty_or_whitespace_error<E>(&self) -> Self::Error<E> {
        PackageVersionError::<E, Self>::EmptyOrWhitespaceOnly(self.clone())
    }

    #[inline]
    fn make_custom_validation_error<EInner: Into<ECustom>, ECustom>(
        &self,
        inner: EInner,
    ) -> Self::Error<ECustom> {
        PackageVersionError::<ECustom, Self>::Other(inner.into())
    }
}

/// Common errors involving package names themselves (not further processing of them like missing
/// package versions or similar, but primarily issues with validity).
///
/// Can provide a custom error which defaults to [`anyhow::Error`]. Because we want to leave the
/// door open for a more structured form of [`PackageVersion`], this error type is generic over
/// both custom errors and over version type.
#[derive(Debug, thiserror::Error)]
pub enum PackageVersionError<Custom = anyhow::Error, Version = PackageVersion> {
    #[error("empty or whitespace-only package version '{0}'")]
    EmptyOrWhitespaceOnly(Version),
    #[error("package version '{0}' contains disallowed character '{1}'")]
    ContainsBannedCharacter(Version, char),
    #[error(transparent)]
    Other(#[from] Custom),
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
