//! Module to ease custom validation of [`super::PackageName`] and [`super::PackageVersion`] for
//! your package manager implementation.

pub(crate) mod cratesealed {
    use crate::utils::ntypeutils::SStringNewtype;

    /// Extension Trait Toolbox (internal) building blocks for [`super::StringDataProcessing`]
    pub trait StringDataProcessingInner: SStringNewtype {
        /// Error type, generic over the custom error.
        type Error<CustomError>;

        /// Make an error for a banned character.
        fn make_banned_character_error<E>(&self, found_banned_char: char) -> Self::Error<E>;

        /// Make an error for an empty-when-whitespace-stripped string
        fn make_empty_or_whitespace_error<E>(&self) -> Self::Error<E>;

        /// Make a "custom validation" error from another error convertble to the custom error
        fn make_custom_validation_error<EInner: Into<ECustom>, ECustom>(
            &self,
            inner: EInner,
        ) -> Self::Error<ECustom>;
    }
}

pub(crate) use cratesealed::StringDataProcessingInner;

use crate::{commontypes::SString, utils::strutils::StrExt as _};

/// Toolbox for validating and processing various string-like structured types
pub trait StringDataProcessing: StringDataProcessingInner {
    /// Trim whitespace off the data.
    ///
    /// If you want to validate that the data is non-empty, see
    /// [`StringDataProcessing::trim_and_validate_nonempty`] - it's more efficient and avoids
    /// copying if the data fails validation.
    #[inline]
    fn trim(&self) -> Self
    where
        Self: Sized,
    {
        Self::from_sstring(SString::from_str(self.as_str().trim()))
    }

    /// Trim whitespace off the data, and validate if it's not empty. If it isn't empty, then
    /// return the trimmed version of the data. If it is, spit out an error.
    #[inline]
    fn trim_and_validate_nonempty<E>(&self) -> Result<Self, Self::Error<E>>
    where
        Self: Sized,
    {
        let trimmed = self.as_str();
        if trimmed.is_empty() {
            Err(self.make_empty_or_whitespace_error())
        } else {
            Ok(Self::from_str_ref(trimmed))
        }
    }

    /// Ban characters from the data - if the data contains the character, then this
    /// returns [`Result::Err`], else [`Result::Ok`].
    ///
    /// The passed function should return `true` for the banned character(s), false otherwise.
    #[inline]
    fn ban_character<E>(&self, banned: impl FnMut(char) -> bool) -> Result<(), Self::Error<E>> {
        match self
            .as_str()
            .find_first_char_matching(banned)
            .map(|banned_char| self.make_banned_character_error(banned_char))
        {
            Some(e) => Err(e),
            None => Ok(()),
        }
    }

    /// Validate the data using a custom function, that generates the custom error you
    /// want. This is converted to whatever the unified error type is
    /// (very likely, [`anyhow::Error`])
    #[inline]
    fn custom_validate_error<E, InnerE: Into<E>>(
        &self,
        error_gen: impl FnOnce(&Self) -> Result<(), InnerE>,
    ) -> Result<(), Self::Error<E>> {
        error_gen(self).map_err(|e| self.make_custom_validation_error(e))
    }

    /// Validate with the overall error type.
    #[inline]
    fn bare_validate_error<E>(
        &self,
        error_gen: impl FnOnce(&Self) -> Result<(), Self::Error<E>>,
    ) -> Result<(), Self::Error<E>> {
        error_gen(self)
    }
}

impl<T: StringDataProcessingInner> StringDataProcessing for T {}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
