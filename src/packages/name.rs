//! Module for package names.
//!
//! Unlike [package versions][super::version], it's unlikely for a package manager to want to
//! provide more structured information on this in future. It might still be necessary, but I'm
//! willing to commit specifically to a package name type.
//!
//! Package manager implementations can do their own validation of package names - either in the
//! software or directly via analyzing package manager data (from CLI or other invocation method).
//! Tools to help with validation can be found in [super::validation].

use core::{
    borrow::Borrow,
    fmt::{Debug, Display},
    str::FromStr,
};

use super::validation;
use crate::{commontypes::SString, utils::ntypeutils::SStringNewtype};

/// Raw package name string, free of any sort of version information. Version information should be
/// added later via [package versions][`super::version`].
///
/// Common package name validation errors - for returning by implementations - can be found at
/// [`PackageNameError`]
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct PackageName(SString);

impl PackageName {
    #[inline]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    #[inline]
    pub fn into_inner(self) -> SString {
        self.0
    }
}

impl SStringNewtype for PackageName {
    #[inline]
    fn into_inner_sstring(self) -> SString {
        self.into_inner()
    }

    #[inline]
    fn from_sstring(value: SString) -> Self {
        Self::from(value)
    }
}

impl validation::StringDataProcessingInner for PackageName {
    type Error<CustomError> = PackageNameError<CustomError>;

    #[inline]
    fn make_banned_character_error<E>(&self, found_banned_char: char) -> Self::Error<E> {
        PackageNameError::<E>::ContainsBannedCharacter(self.clone(), found_banned_char)
    }

    #[inline]
    fn make_empty_or_whitespace_error<E>(&self) -> Self::Error<E> {
        PackageNameError::<E>::EmptyOrWhitespaceOnly(self.clone())
    }

    #[inline]
    fn make_custom_validation_error<EInner: Into<ECustom>, ECustom>(
        &self,
        inner: EInner,
    ) -> Self::Error<ECustom> {
        PackageNameError::<ECustom>::Other(inner.into())
    }
}

impl Display for PackageName {
    #[inline]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

impl From<SString> for PackageName {
    #[inline]
    fn from(value: SString) -> Self {
        PackageName(value)
    }
}

impl From<String> for PackageName {
    #[inline]
    fn from(value: String) -> Self {
        Self::from(SString::from_string(value))
    }
}

impl From<&str> for PackageName {
    #[inline]
    fn from(value: &str) -> Self {
        Self::from(SString::from_str(value))
    }
}

impl FromStr for PackageName {
    type Err = ();

    #[inline]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self::from(s))
    }
}

impl AsRef<str> for PackageName {
    #[inline]
    fn as_ref(&self) -> &str {
        self.as_str()
    }
}

impl Borrow<str> for PackageName {
    #[inline]
    fn borrow(&self) -> &str {
        self.as_str()
    }
}

/// Errors involving package names themselves (not further processing of them like missing packages
/// etc., but primarily issues with validity).
///
/// Can provide a custom error which defaults to [`anyhow::Error`].
#[derive(Debug, thiserror::Error)]
pub enum PackageNameError<Custom = anyhow::Error> {
    #[error("empty or whitespace-only package name '{0}'")]
    EmptyOrWhitespaceOnly(PackageName),
    #[error("package name '{0}' contains disallowed character '{1}'")]
    ContainsBannedCharacter(PackageName, char),
    #[error(transparent)]
    Other(#[from] Custom),
}

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
