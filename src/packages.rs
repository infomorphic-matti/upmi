//! Specification and information for packages, constraints on them, and version information.
//!
//! ## Note on Packages
//! Different package managers and vendors have different standards for packages and versioning of
//! them, as well as how to compare them - in particular, most versionings are not a simple
//! alphanumeric + lexicographic sorting of version strings.
//!
//! For example, the manual for `pacman` package versions and ordering looks something more like:
//! ```txt
//! You can also use pacman -Su to upgrade all packages that are out-of-date. See Sync
//! Options below. When upgrading, pacman performs version comparison to determine which
//! packages need upgrading. This behavior operates as follows:
//!
//!     Alphanumeric:
//!       1.0a < 1.0b < 1.0beta < 1.0p < 1.0pre < 1.0rc < 1.0 < 1.0.a < 1.0.1
//!     Numeric:
//!       1 < 1.0 < 1.1 < 1.1.1 < 1.2 < 2.0 < 3.0.0
//!
//! Additionally, version strings can have an epoch value defined that will overrule any
//! version comparison, unless the epoch values are equal. This is specified in an
//! epoch:version-rel format. For example, 2:1.0-1 is always greater than 1:3.6-1.
//! ```
//! (from `man 8 pacman`)
//!
//! Package managers also often let you constrain versions - for instance, specifying exact
//! versions, or specifying that a package must be `>=`, `>`, `<` or `<=` a given version. For now,
//! we don't want to implement all this stuff in software again.
//!
//! This means we can't easily use something as simple as `semver` as a versioning representation.
//! The most important part is to get the library working first, which means we will begin with
//! bare string representation, but be generic enough that in future we can allow specific
//! implementations to also provide more structured versioning information.
//!
//! It also means that validation is extra difficult. To deal with this:
//! * We provide tools in the [`validation`] module to ease validating individual
//!     [`PackageName`] or [`PackageVersion`] values
//! * We allow package managers to specify how to validate package names and versions in a flexible
//!   way - see [`crate::managers::validating`]
//! * We allow types that contain package names and versions to emit lists of them for validation,
//!   in the form of iterators, via a trait. See [`with_packages`].

pub mod constraint;
pub mod name;
pub mod validation;
pub mod version;
pub mod with_packages;

pub use name::PackageName;
pub use version::PackageVersion;

// upmi - invoke different package managers programmatically
// Copyright (C) 2024  Matti Bryce <mattibryce@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
