# `upmi` - Universal Package Manager Interface
This library provides a means to programmatically interface with numerous operating-system package managers.

It is based on a lot of the work in [upt][upt] - but more designed for structured, programmatic usage.

The library is licensed under AGPLv3 or a later version. 

[upt]: https://github.com/sigoden/upt

